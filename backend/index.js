const express = require('express')
const app = express()
const StudentRouter = require('./routes/index')


app.use(express.json())

app.listen(3000, ()=>console.log('listening in port 3000'))

app.use("/student", StudentRouter)