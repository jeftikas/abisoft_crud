const db = require('../models/index')
const fs = require('fs');
const { Model } = require('sequelize')

const create = async (data) => {
  const response = await db.Student.create(data)
  return response
}

const update = async (data, id) => {
   const response= await db.Student.update(data, {
    where: {
      id
    }
  })
  return response
}

const remove = async (id) => {
  const response = await db.Student.findByPk(id)
  if (response === null) {
    const error = new Error(`User with id ${id} not found`)
    error.status = 404
    throw error
  }
   await db.Student.destroy({ where: { id } })
}

const getAll = async () => {
  const response = await db.Student.findAll({
    order: [['createdAt', 'DESC']],
    attributes: ["name","age"]
  })
  console.log(response);
  return response
}
const getById = async (id) => {
  const response = await db.Student.findByPk(id)
  return response
}

module.exports={
    create,
    update,
    remove,
    getAll,
    getById
}